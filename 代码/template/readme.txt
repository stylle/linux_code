一、初始化部分 register_chrdev_func
1、platform总线注册
2、Of_MATCH_COMPATILBLE     设备树compatilble属性 
3、ID_TABLE_COMPATILBLE     id_table compatilble属性
4、通过两个匹配属性进入probe函数获取设备树信息
5、probe函数注册字符，当insmod方式注册驱动如带有设备号参数则采用静态注册，当未传入设备号采用动态分配
creat_class_and_device_func
6、创建class类、device设备，通过该结构体创建设备节点DEVICE_NODE_NAME 在/dev/下查看

二、中断部分 creat_interrup_func
1、中断部分涉及了三个服务函数分别是中断上文、tasklet、workqueue
static irqreturn_t irq_top_handle(int irq, void *args)
2、中断上文中处理紧急任务且不耗时的操作，并且下文也在上文服务函数中进行调度
void tasklet_down_handle(unsigned long data)
3、中断下文tasklet可以处理一些不紧急的事情
void work_queue_handle(struct work_struct *work)
4、中断下文workqueue队列中可以处理一些更不紧急的事情且可以使用sleep进入睡眠
5、workqueue中还有一个delay_work工作项可以在延时一段时间后进入服务函数
6、目前存在一个问题就是延时工作项无法传参:20230909修复 延时工作队列获取参数的方法：先找到延时工作项的结构体再去找work_data

三、定时器部分
static void timer_handle(unsigned long data)
1、定时器部分相对比较简单 有一个服务函数 
2、启动定时器只需要配置好时间和服务函数后调用add_timer即可启动
3、需要注意在服务函数中需要再次调用mod_timer来重新启动定时器并设置时间

四、应用层操作接口
1、应用层在该模板中写了五个接口分别是read、write、relase、ioctl、open
2、chardev_open在设备节点被应用层以文件的方式打开的时候调用
3、chardev_release在设备节点被应用层以文件的方式释放的时候调用
4、chardev_read应用层调用read接口时调用，驱动层通过copy_to_user返回数据给应用层
5、chardev_write应用层调用write接口时调用，驱动层通过copy_from_user获取应用层传递的数据
6、chardev_ioctl应用层调用iotcl接口是调用，驱动层通过传递的cmd做出相应的判断对传入的数据data进行操作加工
7、详细的使用请参考app.c

五、驱动层主动通知应用层
1、要想在应用层实现像中断一样的主动通知就需要用到信号
2、驱动层要通知应用层需要获得应用层的pid号，模板中通过iotcl设置
3、kernel_to_app_signal有两个参数分别是app的pid和struct siginfo信息结构体
4、在应用层中使用sigaction结构体来实现接受通知
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = &signal_handler;  // 服务函数
    sa.sa_flags = SA_SIGINFO;   // 信号类型
    sigaction(SIGIO, &sa, NULL); // 绑定信号

六、总结
1、如果你不想使用template这个名字，在该模板中所有的取名都是带有template所以我们只需要将template替换成我们的驱动名即可完成全局替换
例如：  template-》mpu6050
2、对于一些系统文件系统中生成的名字都可以在include文件中进行修改方便高效，而我们要完成自己的驱动只需要去主要关注五个供应用程序调用的接口即可


/*
leds {
    compatible = "gpio-leds";
    blue_led {
            label = "licheepi:blue:usr";
            gpios = <&pio 6 1 GPIO_ACTIVE_LOW>; /* PG1 */
    };
    green_led {
            label = "licheepi:green:usr";
            gpios = <&pio 6 0 GPIO_ACTIVE_LOW>; /* PG0 */
            default-state = "on";
    };
    red_led {
            label = "licheepi:red:usr";
            gpios = <&pio 6 2 GPIO_ACTIVE_LOW>; /* PG2 */
    };
};

btns {
    /* custom button  */
    compatible = "licheepi, btns";
    gpios = <&pio 1 5 GPIO_ACTIVE_LOW>; /* PB5 */
    status = "okay";
}
*/