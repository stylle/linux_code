#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/delay.h>

#include <linux/timer.h>

struct timer_list timer_test;

//定义 function_test 定时功能函数
static void timer_handle(unsigned long data)
{
    static int count = 0;
    printk("timer handle event! data = %ld, count = %d\n", data, count++);
   
    if(count < 10)
    {
        //使用 mod_timer 函数将定时时间设置为1秒执行一次
        mod_timer(&timer_test,jiffies_64 + msecs_to_jiffies(1000));
    }
}

/*
 * @description : 驱动入口函数
 * @param       : 无
 * @return      : 无
 */
static int timer_driver_init(void)
{
    init_timer(&timer_test);
    timer_test.expires = jiffies_64 + msecs_to_jiffies(1000);//将定时时间设置1秒执行
    timer_test.function = timer_handle;
    timer_test.data = 123;
    add_timer(&timer_test);//添加一个定时器

    printk(KERN_EMERG "timer_driver_register succeed!\n");
    return 0;
}

/*
 * @description : 驱动出口函数
 * @param       : 无
 * @return      : 无
 */
static void timer_driver_exit(void)
{
    del_timer(&timer_test);//删除一个定时器
    printk(KERN_EMERG "timer driver exit!!!\n");
}

module_init(timer_driver_init);
module_exit(timer_driver_exit);

MODULE_LICENSE("GPL");              //声明模块拥有开源许可
MODULE_AUTHOR("xxxx");              //声明作者
