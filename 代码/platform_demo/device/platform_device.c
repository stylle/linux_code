
#include <linux/init.h> //初始化头文件
#include <linux/module.h> //最基本的文件，支持动态添加和卸载模块。
#include <linux/platform_device.h> //平台设备所需要的头文件
#include <linux/gpio.h>


/**
* @description: 释放 flatform 设备模块的时候此函数会执行
* @param {structdevice} *dev:要释放的设备
* @return {*}
*/
void platform_device_release(struct device *dev)
{
    printk("%s %d\n", __FUNCTION__, __LINE__);
}

// 设备资源信息
struct resource platform_device_res[] = {
    [0] = { 
        .start = 0xfdd60000,  // 对应开始地址
        .end = 0xfdd60003, // 对应结束地址
        .flags = IORESOURCE_MEM,  // 表示GPIO内存
        .name = "GPIO_DR",  // 表示DR寄存器随意取
        } ,
    [1] = { 
        .start = 0xfdd60004,  // 对应第二个驱动使用的开始地址
        .end = 0xfdd60007, // 对应第二个驱动使用的结束地址
        .flags = IORESOURCE_MEM,  // 表示GPIO内存
        .name = "GPIOA7",  // 表示DR寄存器随意取
       }
};

// platform 设备结构体
struct platform_device platform_dev = {
    .name = "device_platform",  // 匹配driever的name
    .id = -1, 
    .resource = platform_device_res, 
    .num_resources = ARRAY_SIZE(platform_device_res), 
    .dev = {
        .release = platform_device_release
    }
};

/**
* @description: 设备模块加载
* @param {*}无
* @return {*}无
*/
static int platform_device_init(void)
{
    // 设备信息注册到 Linux 内核
    platform_device_register(&platform_dev);
    printk("platform_device_register ok \n");
    return 0;
}

/**
* @description: 设备模块注销
* @param {*}无
* @return {*}无
*/
static void platform_device_exit(void)
{
    // 设备信息卸载
    platform_device_unregister(&platform_dev);
    printk("%s %d\n", __FUNCTION__, __LINE__);
}

module_init(platform_device_init);
module_exit(platform_device_exit);

MODULE_AUTHOR("Kongjun"); // 作者
MODULE_DESCRIPTION("platform driver"); // 驱动介绍可以不写
MODULE_LICENSE("GPL");// 开源协议
