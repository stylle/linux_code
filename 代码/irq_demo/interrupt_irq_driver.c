#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/delay.h>

//interrupt
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/workqueue.h>
#include <linux/delay.h>


int irq;

// struct tasklet_struct tasklet_test;

// 共享工作队列
struct work_struct work_test; // 共享工作队列的工作项

// 自定义工作队列
// struct workqueue_struct *work_queue_test; // 工作队列 
// struct work_struct work_test;  // 工作项

// 延时工作队列
// struct workqueue_struct *work_queue_test;  // 自定义工作队列
// struct delayed_work delay_work_test; // 延时工作项

// 并发管理工作队列
// struct workqueue_struct *work_queue_test; // 工作队列 
// struct work_struct work_test;  // 工作项

// 工作队列传参
// struct work_data{
//     struct work_struct work;
//     char name[12];
// };
// struct workqueue_struct *work_queue_test; // 工作队列
// struct work_data work_test;  // 工作项 

/*
 * @description     : 打开设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 设备文件，file结构体有个叫做private_data的成员变量
 *                    一般在open的时候将private_data指向设备结构体。
 * @return          : 0 成功;其他 失败
 */
static int interrupt_irq_open (struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "hello interrupt_irq_open!\n");
    return 0;
}

/*
 * @description     : 关闭/释放设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 要关闭的设备文件(文件描述符)
 * @return          : 0 成功; 其他 失败
 */
static int interrupt_irq_release(struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "bye bye interrupt_irq_release!\n");
    return 0;
}

/*
 * @description     : 从设备读取数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要读取的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t interrupt_irq_read(struct file *file, char __user *ubuf, size_t cnt, loff_t *offt)
{
    int ret;
    char data[] = "hello interrupt_irq!";
    ret = copy_to_user(ubuf, data, sizeof(data));

    printk(KERN_EMERG "hello interrupt_irq_read!\n");
    return ret;
}

/*
 * @description     : 从设备节点写入数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要写入的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t interrupt_irq_write(struct file *file, const char __user *ubuf, size_t cnt, loff_t *offt)
{
    char kbuf[64] = "copy from user!\n";
    if( copy_from_user(kbuf, ubuf, cnt) != 0 ){
        printk(KERN_EMERG "copy_from_user error!\n");
        return -1;
    }
    printk("buf is:%s\n", kbuf);

    printk(KERN_EMERG "hello interrupt_irq_write!\n");
    return 0;
}

/* 操作函数结构体 */
struct file_operations interrupt_irq_fops = {
    .owner      = THIS_MODULE,
    .open       = interrupt_irq_open,
    .release    = interrupt_irq_release,
    .read       = interrupt_irq_read,
    .write      = interrupt_irq_write,
};

/* 杂项设备结构体 */
struct miscdevice interrupt_irq_dev = {
    .minor  = MISC_DYNAMIC_MINOR,     /* 次设备号 */
    .name   = "interrupt_irq_name",            /* 设备名/dev/下的设备节点名   */
    .fops   = &interrupt_irq_fops              /* 设备文件操作方法 */
};


// // down 处理不紧急或者耗时的事情,禁止使用休眠函数sleep否则系统将会崩溃
// void interrupt_irq_tasklet_down_handle(unsigned long data)
// {
//     printk("interrupt_irq_tasklet_down_handle data:%ld\n", data);   
// }

// top half 处理紧急的事情
static irqreturn_t interrupt_irq_top_handle(int irq, void *args)
{
    printk("interrupt_irq_top_handle\n");

    // 中断下文tasklet调度
    // tasklet_schedule(&tasklet_test);

    // 共享工作队列调度
    schedule_work(&work_test); 

    // 自定义工作队列调度
    // queue_work(work_queue_test, &work_test); 

    // 延时工作队列调度
    // queue_delayed_work(work_queue_test, &delay_work_test, 3 * HZ);  // HZ表示时钟节拍在内核中配置，一般都是100 表示一秒跳动100次 这里表示3秒
    // queue_delayed_work(work_queue_test, &delay_work_test, 10); // 100ms

    // 并发管理工作队列调度
    // queue_work(work_queue_test, &work_test); 

    // 工作队列传参
    // queue_work(work_queue_test, &work_test.work); 

    return IRQ_RETVAL(IRQ_HANDLED);
}

// 工作项处理函数 可以处理更加不紧急的事情且可以保持休眠使用sleep
void work_queue_handle(struct work_struct *work)
{
    // 工作队列获取参数的方法
    // struct work_data *tmp_work;
    // tmp_work = container_of(work, struct work_data, work);
    // msleep(20); 
    // printk("work_queue_handle work.name = %s\n", tmp_work->name);  

    msleep(20); 
    printk("work_queue_handle \n");   
}

/*
 * @description : 驱动入口函数
 * @param       : 无
 * @return      : 无
 */
static int interrupt_irq_init(void)
{
    int ret;
    int gpio = 1*32+5;// PB5 = 1 * 32 + 5

    // 申请中断号
    irq = gpio_to_irq(gpio); 
    printk(KERN_INFO "gpio_to_irq IRQ = %d!  gpio = %d\n", irq, gpio); 

    // 申请中断
    ret = request_irq(irq, interrupt_irq_top_handle, IRQF_TRIGGER_FALLING, "interrupt_irq_demo", NULL);
    if(ret < 0)
    {
        printk(KERN_EMERG "request_irq error!\n");
        return -1;
    }
	
    // 中断下文tasklet的初始化
	// tasklet_init(&tasklet_test, interrupt_irq_tasklet_down_handle, 1);// data的值也可以不传递也就是这个1

    // 共享工作队列创建
    INIT_WORK(&work_test, work_queue_handle);  // 工作项初始化

    // 自定义工作队列创建
    // work_queue_test = create_workqueue("test work"); // 参数是队列， 返回值为工作队列指针
    // INIT_WORK(&work_test, work_queue_handle);        // 初始化工作项

    // 延时工作队列创建
    // work_queue_test = create_workqueue("test delay work");
    // INIT_DELAYED_WORK(&delay_work_test, work_queue_handle); // 初始化延时工作项

    // 并发管理工作队列
    // work_queue_test = alloc_workqueue("test work", WQ_UNBOUND, 0);
    // INIT_WORK(&work_test, work_queue_handle);        // 初始化工作项

    // 工作队列传参
    // work_queue_test = alloc_workqueue("test work", WQ_UNBOUND, 0);
    // INIT_WORK(&work_test.work, work_queue_handle);        // 初始化工作项
    // sprintf(work_test.name, "hello work");

    // 杂项设备的注册
    ret = misc_register(&interrupt_irq_dev);
    if(ret < 0){
        printk(KERN_EMERG "interrupt_irq_register failed!\n");
        return -1;
    }

    printk(KERN_EMERG "interrupt_irq_register succeed!\n");           // 在内核中无法使用c语言库，所以不用printf
    
    return 0;
}

/*
 * @description : 驱动出口函数
 * @param       : 无
 * @return      : 无
 */
static void interrupt_irq_exit(void)
{
    // 杂项设备注销
    misc_deregister(&interrupt_irq_dev);
	
    // 释放中断号
    free_irq(irq, NULL);
	
    // 中断下文关闭并删除
	// tasklet_enable(&tasklet_test);
	// tasklet_kill(&tasklet_test);

    // 静态创建的工作队列不需要做释放
    cancel_work_sync(&work_test); // 取消工作项

    // 自定义工作队列释放
    // cancel_work_sync(&work_test); // 取消工作项
    // flush_workqueue(work_queue_test);   // 刷新工作队列
    // destroy_workqueue(work_queue_test); // 销毁工作队列

    // 延时工作队列的释放
    // cancel_delayed_work_sync(&delay_work_test); // 取消工作项
    // flush_workqueue(work_queue_test);   // 刷新工作队列
    // destroy_workqueue(work_queue_test); // 销毁工作队列

    // 并发工作队列的释放
    // cancel_work_sync(&work_test); // 取消工作项
    // flush_workqueue(work_queue_test);   // 刷新工作队列
    // destroy_workqueue(work_queue_test); // 销毁工作队列

    // 工作队列传参的释放
    // cancel_work_sync(&work_test.work); // 取消工作项
    // flush_workqueue(work_queue_test);   // 刷新工作队列
    // destroy_workqueue(work_queue_test); // 销毁工作队列


    printk(KERN_EMERG "interrupt_irq exit!!!\n");


}

module_init(interrupt_irq_init);
module_exit(interrupt_irq_exit);

MODULE_LICENSE("GPL");              //声明模块拥有开源许可
MODULE_AUTHOR("xxxx");              //声明作者
