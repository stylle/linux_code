#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/delay.h>

//interrupt
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/workqueue.h>
#include <linux/delay.h>

// 向应用层通知信息
#include <asm/siginfo.h>
#include <linux/pid.h>
#include <linux/uaccess.h>
#include <linux/sched/signal.h>


int irq;

// 用来保存向谁发送信号，应用程序通过 ioctl 把自己的进程 ID 设置进来。
static int g_pid = 0;
#define SET_PID						1

// 共享工作队列
struct delayed_work  delay_work_test; // 共享工作队列的工作项

/*
 * @description     : 打开设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 设备文件，file结构体有个叫做private_data的成员变量
 *                    一般在open的时候将private_data指向设备结构体。
 * @return          : 0 成功;其他 失败
 */
static int interrupt_irq_open (struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "hello interrupt_irq_open!\n");
    return 0;
}

/*
 * @description     : 关闭/释放设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 要关闭的设备文件(文件描述符)
 * @return          : 0 成功; 其他 失败
 */
static int interrupt_irq_release(struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "bye bye interrupt_irq_release!\n");
    return 0;
}

/*
 * @description     : 从设备读取数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要读取的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t interrupt_irq_read(struct file *file, char __user *ubuf, size_t cnt, loff_t *offt)
{
    int ret;
    char data[] = "hello interrupt_irq!";
    ret = copy_to_user(ubuf, data, sizeof(data));

    printk(KERN_EMERG "hello interrupt_irq_read!\n");
    return ret;
}

/*
 * @description     : 从设备节点写入数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要写入的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t interrupt_irq_write(struct file *file, const char __user *ubuf, size_t cnt, loff_t *offt)
{
    char kbuf[64] = "copy from user!\n";
    if( copy_from_user(kbuf, ubuf, cnt) != 0 ){
        printk(KERN_EMERG "copy_from_user error!\n");
        return -1;
    }
    printk("buf is:%s\n", kbuf);

    printk(KERN_EMERG "hello interrupt_irq_write!\n");
    return 0;
}


static long interrupt_irq_ioctl(struct file* file, unsigned int cmd, unsigned long arg)
{
	void __user *pArg;
	
	printk("[kernel] interrupt_irq_ioctl is called. cmd = %d\n", cmd);
	
	if (SET_PID == cmd) // 设置进程号
	{
		pArg = (void *)arg;
        // 检测参数是否正确
		if (!access_ok(int, pArg, sizeof(int)))
		{
		    printk("[kernel] access failed!\n");
		    return -EACCES;
		}

		// 把用户空间的数据复制到内核空间
		if (copy_from_user(&g_pid, pArg, sizeof(int)))
		{
		    printk("[kernel] copy_from_user failed!\n");
		    return -EFAULT;
		}

		printk("[kernel] interrupt_irq_ioctl pid = %d.\n", g_pid);
	}

	return 0;
}


/* 操作函数结构体 */
struct file_operations interrupt_irq_fops = {
    .owner      = THIS_MODULE,
    .open       = interrupt_irq_open,
    .release    = interrupt_irq_release,
    .read       = interrupt_irq_read,
    .write      = interrupt_irq_write,
    .unlocked_ioctl = interrupt_irq_ioctl,
};

/* 杂项设备结构体 */
struct miscdevice interrupt_irq_dev = {
    .minor  = MISC_DYNAMIC_MINOR,     /* 次设备号 */
    .name   = "interrupt_irq_name",            /* 设备名/dev/下的设备节点名   */
    .fops   = &interrupt_irq_fops              /* 设备文件操作方法 */
};

// top half 处理紧急的事情
static irqreturn_t interrupt_irq_top_handle(int irq, void *args)
{
    // 共享工作队列调度
    schedule_delayed_work(&delay_work_test, 10); // 100ms

    return IRQ_RETVAL(IRQ_HANDLED);
}



// 用来发送信号给应用程序
static void send_signal(int sig_no, int sig_code)
{
	int ret;
	struct siginfo info;
	struct task_struct *my_task = NULL;
	
	if (0 == g_pid)
	{
		// 说明应用程序没有设置自己的 PID
	    printk("[kernel] pid[%d] is not valid!\n", g_pid);
	    return;
	}

	printk("[kernel] send signal %d code %d to pid %d.\n", sig_no, sig_code, g_pid);

	// 构造信号结构体
	memset(&info, 0, sizeof(struct siginfo));
	info.si_signo = sig_no;
	info.si_errno = 0;
	info.si_code  = sig_code;

	// 获取自己的任务信息，使用的是 RCU 锁
	rcu_read_lock();
	my_task = pid_task(find_vpid(g_pid), PIDTYPE_PID);
	rcu_read_unlock();

	if (my_task == NULL)
	{
	    printk("get pid_task failed!\n");
	    return;
	}

	// 发送信号
	ret = send_sig_info(sig_no, (struct siginfo *)&info, my_task);
	if (ret < 0) 
	{
	       printk("send signal failed!\n");
	}
}


// 工作项处理函数 可以处理更加不紧急的事情且可以保持休眠使用sleep
void work_queue_handle(struct work_struct *work)
{
    send_signal(SIGIO, 1);
    printk("[kernel] work_queue_handle \n");   
}

/*
 * @description : 驱动入口函数
 * @param       : 无
 * @return      : 无
 */
static int interrupt_irq_init(void)
{
    int ret;
    int gpio = 1*32+5;// PB5 = 1 * 32 + 5

    // 申请中断号
    irq = gpio_to_irq(gpio); 
    printk(KERN_INFO "gpio_to_irq IRQ = %d!  gpio = %d\n", irq, gpio); 

    // 申请中断
    ret = request_irq(irq, interrupt_irq_top_handle, IRQF_TRIGGER_FALLING, "interrupt_irq_demo", NULL);
    if(ret < 0)
    {
        printk(KERN_EMERG "request_irq error!\n");
        return -1;
    }

    // 共享工作项创建
    INIT_DELAYED_WORK(&delay_work_test, work_queue_handle);  // 工作项初始化

    // 杂项设备的注册
    ret = misc_register(&interrupt_irq_dev);
    if(ret < 0){
        printk(KERN_EMERG "interrupt_irq_register failed!\n");
        return -1;
    }

    printk(KERN_EMERG "interrupt_irq_register succeed!\n");           // 在内核中无法使用c语言库，所以不用printf
    
    return 0;
}

/*
 * @description : 驱动出口函数
 * @param       : 无
 * @return      : 无
 */
static void interrupt_irq_exit(void)
{
    // 杂项设备注销
    misc_deregister(&interrupt_irq_dev);
	
    // 释放中断号
    free_irq(irq, NULL);

    // 静态创建的工作队列不需要做释放
    cancel_delayed_work_sync(&delay_work_test); // 取消工作项


    printk(KERN_EMERG "interrupt_irq exit!!!\n");
}

module_init(interrupt_irq_init);
module_exit(interrupt_irq_exit);

MODULE_LICENSE("GPL");              //声明模块拥有开源许可
MODULE_AUTHOR("xxxx");              //声明作者
