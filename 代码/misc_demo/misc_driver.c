#include <linux/init.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/delay.h>


/*
 * @description     : 打开设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 设备文件，file结构体有个叫做private_data的成员变量
 *                    一般在open的时候将private_data指向设备结构体。
 * @return          : 0 成功;其他 失败
 */
static int misc_open (struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "hello misc_open!\n");
    return 0;
}

/*
 * @description     : 关闭/释放设备
 * @param - inode   : 传递给驱动的inode
 * @param - filp    : 要关闭的设备文件(文件描述符)
 * @return          : 0 成功; 其他 失败
 */
static int misc_release(struct inode *inode, struct file *file)
{
    printk(KERN_EMERG "bye bye misc_release!\n");
    return 0;
}

/*
 * @description     : 从设备读取数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要读取的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t misc_read(struct file *file, char __user *ubuf, size_t cnt, loff_t *offt)
{
    int ret;
    char data[12] = "hello misc!";
    ret = copy_to_user(ubuf, data, sizeof(data));

    printk(KERN_EMERG "hello misc_read!\n");
    return ret;
}

/*
 * @description     : 从设备节点写入数据 
 * @param - filp    : 要打开的设备文件(文件描述符)
 * @param - buf     : 返回给用户空间的数据缓冲区
 * @param - cnt     : 要写入的数据长度
 * @param - offt    : 相对于文件首地址的偏移
 * @return          : 读取的字节数，如果为负值，表示读取失败
 */
static ssize_t misc_write(struct file *file, const char __user *ubuf, size_t cnt, loff_t *offt)
{
    char kbuf[64] = "copy from user!\n";
    if( copy_from_user(kbuf, ubuf, cnt) != 0 ){
        printk(KERN_EMERG "copy_from_user error!\n");
        return -1;
    }
    printk("buf is:%s\n", kbuf);

    printk(KERN_EMERG "hello misc_write!\n");
    return 0;
}

/* 操作函数结构体 */
struct file_operations misc_fops = {
    .owner      = THIS_MODULE,
    .open       = misc_open,
    .release    = misc_release,
    .read       = misc_read,
    .write      = misc_write,
};

/* 杂项设备结构体 */
struct miscdevice misc_dev = {
    .minor  = MISC_DYNAMIC_MINOR,     /* 次设备号 */
    .name   = "misc_name",            /* 设备名/dev/下的设备节点名   */
    .fops   = &misc_fops              /* 设备文件操作方法 */
};

/*
 * @description : 驱动入口函数
 * @param       : 无
 * @return      : 无
 */
static int misc_init(void)
{
    int ret;
    ret = misc_register(&misc_dev);

    if(ret < 0){
        printk(KERN_EMERG "misc_register failed!\n");
        return -1;
    }

    printk(KERN_EMERG "misc_register succeed!\n");           // 在内核中无法使用c语言库，所以不用printf
    return 0;
}

/*
 * @description : 驱动出口函数
 * @param       : 无
 * @return      : 无
 */
static void misc_exit(void)
{
    misc_deregister(&misc_dev);
    printk(KERN_EMERG "misc exit!!!\n");
}

module_init(misc_init);
module_exit(misc_exit);

MODULE_LICENSE("GPL");              //声明模块拥有开源许可
MODULE_AUTHOR("xxxx");              //声明作者
