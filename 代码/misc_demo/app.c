#include "stdio.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/ioctl.h"
#include "fcntl.h"
#include "stdlib.h"
#include "string.h"
#include <poll.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <fcntl.h>

/*
 * @description        : main主程序
 * @param - argc     : argv数组元素个数
 * @param - argv     : 具体参数
 * @return             : 0 成功;其他 失败
 */
 
int main(int argc, char *argv[])
{
    int fd;
    char *filename;
    int ret = 0;
    char databuf[12];
 
    // if (argc != 2) {
    //     printf("Error Usage!\r\n");
    //     return -1;
    // }
 
    // filename = argv[1];
    filename = "/dev/misc_name";
    fd = open(filename, O_RDWR);
    if(fd < 0) 
    {
        printf("can't open file %s\r\n", filename);
        return -1;
    }

    ret = read(fd, databuf, sizeof(databuf));
    if(ret == 0) 
    {             
        printf("read: %s\n", databuf);
    }

    close(fd);    /* 关闭文件 */    
    return 0;
}
 
 
 
