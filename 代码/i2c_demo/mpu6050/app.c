#include "stdio.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/ioctl.h"
#include "fcntl.h"
#include "stdlib.h"
#include "string.h"
#include <poll.h>
#include <sys/select.h>
#include <sys/time.h>
#include <signal.h>
#include <fcntl.h>
 
#define m 0.0002663
#define n 0.0001211
 
/*
 * @description        : main主程序
 * @param - argc     : argv数组元素个数
 * @param - argv     : 具体参数
 * @return             : 0 成功;其他 失败
 */
 
int main(int argc, char *argv[])
{
    int fd;
    char *filename;
    short databuf[7];
    short a[3], g[3], tem;
    unsigned char name,bu[1];
    float temperature;
    int ret = 0;
 
    // filename = argv[1];
    filename = "/dev/mpu6050";
    fd = open(filename, O_RDWR);
    if(fd < 0) 
    {
        printf("can't open file %s\r\n", filename);
        return -1;
    }
 
    while (1) 
    {
        ret = read(fd, databuf, sizeof(databuf));
        if(ret == 0) 
        {        
            a[0] = databuf[0];
            a[1] = databuf[1];
            a[2] = databuf[2];
 
            tem = databuf[3];
            temperature = 36.53+((int)tem)/340;
 
            g[0] = databuf[4];
            g[1] = databuf[5];
            g[2] = databuf[6];
 
            printf("ax=%4fg ay=%4fg az=%4fg\n",(float)(a[0]*n),(float)(a[1]*n),(float)(a[2]*n));
            printf("gx=%4fr/s gy=%4fr/s gz=%4fr/s\n",(float)(g[0]*m),(float)(g[1]*m),(float)(g[2]*m));
            printf("temprature=%f\n",temperature);
        }
        usleep(200000); /*100ms */
    }
    close(fd);    /* 关闭文件 */    
    return 0;
}
 
 
 