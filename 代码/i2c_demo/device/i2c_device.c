
#include <linux/init.h> //初始化头文件
#include <linux/module.h> //最基本的文件，支持动态添加和卸载模块。
#include <linux/i2c.h> //平台设备所需要的头文件


static struct i2c_client *mpu6050_dev;
static struct i2c_board_info mpu6050_dev_info = {
    .type = "mpu6050",
    .addr = 0x69,
};

/**
* @description: 设备模块加载
* @param {*}无
* @return {*}无
*/
static int i2c_device_init(void)
{
    struct i2c_adapter *adap;
    adap = i2c_get_adapter(0); // i2c0 /sys/bus/i2c/devices -> 0-0069 0对应i2c0  0069两个字节的地址位
    mpu6050_dev = i2c_new_device(adap, &mpu6050_dev_info);
    i2c_put_adapter(adap);
    printk("[kernel] %s %d\n", __FUNCTION__, __LINE__);
    return 0;
}

/**
* @description: 设备模块注销
* @param {*}无
* @return {*}无
*/
static void i2c_device_exit(void)
{
    // 设备信息卸载
    if(mpu6050_dev != NULL)
    {
        i2c_unregister_device(mpu6050_dev);
    }
    printk("[kernel] %s %d\n", __FUNCTION__, __LINE__);
}

module_init(i2c_device_init);
module_exit(i2c_device_exit);

MODULE_AUTHOR("Kongjun"); // 作者
MODULE_DESCRIPTION("i2c device"); // 驱动介绍可以不写
MODULE_LICENSE("GPL");// 开源协议
