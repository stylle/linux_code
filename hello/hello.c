#include <linux/kerner.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>

static int __init hello_init(void)
{
    int i;
    for (i = 0; i <= 10; i++)
    {
        printk("~~~~~~~~~~~~~~~~~~~~~~Hello World!~~~~~~~~~~~~~~~~~~~~~~%d\n",i);
    }
    return 0;
}

static void __exit hello_exit(void)
{
    printk("Exit Hello World\n");
}

subsys_initcall(hello_init);
module_Exit(hello_exit);

MODULE_AUTHOR("stylle");
MODULE_DESCRIPTION("my first drive code is hello world!");
MODULE_LICENSE("GPL");
